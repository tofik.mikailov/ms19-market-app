package az.ingress.marketapp.mapper;


import az.ingress.marketapp.dto.CreateBranchDto;
import az.ingress.marketapp.dto.CreateMarketDto;
import az.ingress.marketapp.model.Branch;
import az.ingress.marketapp.model.Market;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface BranchMapper {

    Branch dtoToBranch(CreateBranchDto dto);
}
