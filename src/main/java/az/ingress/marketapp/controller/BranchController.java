package az.ingress.marketapp.controller;

import az.ingress.marketapp.dto.CreateBranchDto;
import az.ingress.marketapp.model.Branch;
import az.ingress.marketapp.repository.genericsearch.SearchCriteria;
import az.ingress.marketapp.service.BranchService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/market/{marketId}/branch")
@Valid
public class BranchController {

    private final BranchService branchService;

    @PostMapping
    public void create(@PathVariable Long marketId,
                       @RequestBody CreateBranchDto branchDto) {
        branchService.create(marketId, branchDto);
    }

    @PostMapping("/search")
    public Page<Branch> search(@PathVariable Long marketId,
                               @RequestBody List<SearchCriteria> searchCriteria,
                               Pageable pageable) {
        return branchService.searchByName(marketId, searchCriteria, pageable);
    }
}
