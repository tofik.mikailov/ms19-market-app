package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.CreateBranchDto;
import az.ingress.marketapp.mapper.BranchMapper;
import az.ingress.marketapp.model.Branch;
import az.ingress.marketapp.model.Market;
import az.ingress.marketapp.repository.BranchRepository;
import az.ingress.marketapp.repository.MarketRepository;
import az.ingress.marketapp.repository.genericsearch.CustomSpecification;
import az.ingress.marketapp.repository.genericsearch.SearchCriteria;
import jakarta.persistence.criteria.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {

    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final BranchMapper branchMapper;

    @Override
    public void create(Long marketId, CreateBranchDto branchDto) {
        Market market = marketRepository.findById(marketId).orElseThrow(RuntimeException::new);
        Branch branch = branchMapper.dtoToBranch(branchDto);
        branch.setMarket(market);
        branchRepository.save(branch);
    }

    @Override
    public Page<Branch> searchByName(Long marketId, List<SearchCriteria> searchCriteria, Pageable pageable) {
        List<SearchCriteria> newCriteria = searchCriteria.stream()
                .filter(criteria -> !criteria.getKey().equals("name")).toList();
        CustomSpecification<Branch> customSpecification = new CustomSpecification<>(newCriteria);

        Specification<Branch> marketIdSpec = (root, query, criteriaBuilder) -> {
            Join<Branch, Market> marketBranches = root.join("market");
            return criteriaBuilder.equal(marketBranches.get("id"), marketId);
        };
        return branchRepository.findAll(customSpecification.and(marketIdSpec), pageable);
    }
}
