package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.CreateBranchDto;
import az.ingress.marketapp.model.Branch;
import az.ingress.marketapp.repository.genericsearch.SearchCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BranchService {
     void create(Long marketId, CreateBranchDto branchDto);

    Page<Branch> searchByName(Long marketId, List<SearchCriteria> searchCriteria, Pageable pageable);
}
