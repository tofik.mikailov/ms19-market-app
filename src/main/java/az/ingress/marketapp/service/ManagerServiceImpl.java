package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.CreateManagerDto;
import az.ingress.marketapp.mapper.ManagerMapper;
import az.ingress.marketapp.model.Manager;
import az.ingress.marketapp.repository.BranchRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ManagerServiceImpl implements ManagerService {
    private final BranchRepository branchRepository;
    private final ManagerMapper managerMapper;

    @Override
    public void create(Long marketId, Long branchId, CreateManagerDto managerDto) {
        var branch = branchRepository.findById(branchId).orElseThrow(RuntimeException::new);
        if (!branch.getMarket().getId().equals(marketId)) {
            throw new RuntimeException();
        }
        Manager manager = managerMapper.dtoToManager(managerDto);
        branch.setManager(manager);
        branchRepository.save(branch);
    }
}
