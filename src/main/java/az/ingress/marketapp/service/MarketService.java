package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.CreateMarketDto;
import az.ingress.marketapp.dto.MarketDto;
import az.ingress.marketapp.model.Market;
import az.ingress.marketapp.repository.genericsearch.SearchCriteria;

import java.util.Collection;
import java.util.List;

public interface MarketService {
    void create(CreateMarketDto marketDto);

    List<Market> findAll();

    MarketDto findById(Long id);

    void update(Long id, CreateMarketDto marketDto);

    void delete(Long id);

    Collection<Market> searchByName(List<SearchCriteria> searchCriteria);

}
