package az.ingress.marketapp.repository;

import az.ingress.marketapp.model.Branch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public interface BranchRepository extends JpaRepository<Branch, Long>, JpaSpecificationExecutor<Branch> {

    @Query(value = "from Branch b join b.market m where m.id = :marketId and b.name = :name")
    Collection<Branch> findAllBranchesByName(String name, Long marketId);

    @Query(value = "select * from branch",
            countQuery = "SELECT COUNT(*) from branch",
            nativeQuery = true)
    Page<Branch> findAllCustom(Pageable pageable);
}
