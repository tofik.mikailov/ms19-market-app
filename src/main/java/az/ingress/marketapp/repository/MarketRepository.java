package az.ingress.marketapp.repository;

import az.ingress.marketapp.model.Market;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface MarketRepository extends JpaRepository<Market, Long> , JpaSpecificationExecutor<Market> {
    Optional<Market> findByName(String name);
}
