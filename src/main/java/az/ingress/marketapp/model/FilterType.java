package az.ingress.marketapp.model;

public enum FilterType {
    EQUALS,
    START_WITH,
    ENDS_WITH
}
